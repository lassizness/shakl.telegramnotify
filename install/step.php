<?php

CAdminMessage::ShowMessage([
    "TYPE" => "OK",
    "MESSAGE" => "Модуль shakl.telegramnotify успешно установлен.",
    "DETAILS" => "Версия модуля: " . $this->MODULE_VERSION,
    "HTML" => true
]);