<?php

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$data = json_decode(file_get_contents("php://input"), true);

// Вместо использования функции, мы можем вызвать метод класса напрямую
TelegramNotify::logData($data);

if(isset($data['message'])) {
    $handler = new WebhookHandler();
    $handler->handle($data);
}

