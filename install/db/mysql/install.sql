CREATE TABLE `b_user_telegram` (
                                   `USER_ID` INT(11) NOT NULL,
                                   `CHAT_ID` VARCHAR(255) NOT NULL,
                                   `VERIFICATION_CODE` VARCHAR(255) NULL DEFAULT NULL, -- Добавленное поле
                                   PRIMARY KEY (`USER_ID`),
                                   INDEX `IDX_VERIFICATION_CODE` (`VERIFICATION_CODE`) -- Добавленный индекс
);
