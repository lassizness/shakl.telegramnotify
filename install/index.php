<?php

use Bitrix\Main\ModuleManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\EventManager;

class shakl_telegramnotify extends CModule
{

    public function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . "/version.php");
        $this->MODULE_ID = 'shakl.telegramnotify';
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = "ШАКЛ - Telegram Notifications";
        $this->MODULE_DESCRIPTION = "Send notifications to users via Telegram.";
        $this->PARTNER_NAME = "SHAKL tech";
        $this->PARTNER_URI = "https://shakl.tech";
    }


    public function DoInstall()
    {
        if (!ModuleManager::isModuleInstalled($this->MODULE_ID)) {
            ModuleManager::registerModule($this->MODULE_ID);
        }
        $conn = Application::getConnection();
        $tableName = 'b_user_telegram'; // замените 'your_table_name' на реальное имя вашей таблицы

// Проверка существования таблицы
        $tableExists = $conn->query("SHOW TABLES LIKE '$tableName'")->fetch();

        if (!$tableExists) {
            $conn->queryExecute(file_get_contents(__DIR__ . '/db/mysql/install.sql'));
        }

        $defaultEvents = [
            'sale' => [
                ["event" => "OnSaleOrderAdd", "class" => \Bitrix\Sale\Order::class],
                ["event" => "OnSaleOrderUpdate", "class" => \Bitrix\Sale\Order::class],
                ["event" => "OnSaleOrderDelete", "class" => \Bitrix\Sale\Order::class],
                ["event" => "OnSaleOrderPaid", "class" => \Bitrix\Sale\Order::class],
                ["event" => "OnSaleOrderCanceled", "class" => \Bitrix\Sale\Order::class],
                ["event" => "OnSaleOrderDeliver", "class" => \Bitrix\Sale\Order::class],
                ["event" => "OnSaleOrderFinalStatus", "class" => \Bitrix\Sale\Order::class],
                ["event" => "OnSaleBasketItemRefreshData", "class" => \Bitrix\Sale\BasketItem::class],
                ["event" => "OnSaleBasketItemEntitySaved", "class" => \Bitrix\Sale\BasketItem::class],
                ["event" => "OnSaleBasketItemDeleted", "class" => \Bitrix\Sale\BasketItem::class],
                ["event" => "OnSaleOrderBeforeSaved", "class" => \Bitrix\Sale\Order::class],
                ["event" => "OnSaleOrderSaved", "class" => \Bitrix\Sale\Order::class],
                ["event" => "OnSaleOrderEntitySaved", "class" => \Bitrix\Sale\Order::class],
                ["event" => "OnSalePaymentEntitySaved", "class" => \Bitrix\Sale\Payment::class],
                ["event" => "OnSaleShipmentEntitySaved", "class" => \Bitrix\Sale\Shipment::class],
                ["event" => "OnSaleShipmentItemEntitySaved", "class" => \Bitrix\Sale\ShipmentItem::class],
                ["event" => "OnSaleShipmentItemStoreEntitySaved", "class" => null],
                ["event" => "OnSaleOrderPropsValueEntitySaved", "class" => \Bitrix\Sale\PropertyValue::class],
                ["event" => "OnSaleTaxForOrder", "class" => null],
                ["event" => "OnSaleTaxRateLocationCheck", "class" => null],
                ["event" => "OnBasketAdd", "class" => \Bitrix\Sale\Basket::class],
                ["event" => "OnBasketDelete", "class" => \Bitrix\Sale\Basket::class],
                ["event" => "OnBasketUpdate", "class" => \Bitrix\Sale\Basket::class],
                ["event" => "OnBeforeBasketAdd", "class" => \Bitrix\Sale\Basket::class],
                ["event" => "OnBeforeBasketDelete", "class" => \Bitrix\Sale\Basket::class],
                ["event" => "OnBeforeBasketUpdate", "class" => \Bitrix\Sale\Basket::class],
                ["event" => "OnBasketOrder", "class" => \Bitrix\Sale\Basket::class],
                ["event" => "OnBeforeBasketOrder", "class" => \Bitrix\Sale\Basket::class],
                ["event" => "OnSaleBasketSaved", "class" => \Bitrix\Sale\Basket::class],
            ],
            ];

        COption::SetOptionString("shakl.telegramnotify", "EVENTS", json_encode($defaultEvents));
        $this->InstallEvents();
        $this->InstallFiles();
        $GLOBALS["APPLICATION"]->IncludeAdminFile("Установка модуля Telegram Notifications", __DIR__ . "/step.php");
    }

    public function DoUninstall()
    {
        if (ModuleManager::isModuleInstalled($this->MODULE_ID)) {
            ModuleManager::unregisterModule($this->MODULE_ID);
        }
        $conn = Application::getConnection();
        $tableName = 'b_user_telegram'; // замените 'your_table_name' на реальное имя вашей таблицы

// Проверка существования таблицы
        $tableExists = $conn->query("SHOW TABLES LIKE '$tableName'")->fetch();

        if ($tableExists) {
           $conn->queryExecute(file_get_contents(__DIR__ . '/db/mysql/uninstall.sql'));
        }
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        // Удаление списка событий
        COption::RemoveOption("shakl.telegramnotify", "EVENTS");
        $GLOBALS["APPLICATION"]->IncludeAdminFile("Деинсталляция модуля Telegram Notifications", __DIR__ . "/unstep.php");
    }

    function InstallEvents()
    {
        Loader::includeModule($this->MODULE_ID);
        EventManager::getInstance()->registerEventHandler("main", "OnBuildGlobalMenu", 'CShaklTelegramNotifyHandlers', 'OnBuildGlobalMenu');
    return true;
    }

    function UnInstallEvents()
    {
        Loader::includeModule($this->MODULE_ID);
        EventManager::getInstance()->unRegisterEventHandler("main", "OnBuildGlobalMenu", 'CShaklTelegramNotifyHandlers','OnBuildGlobalMenu');
        return true;
    }

    function InstallFiles($arParams = array())
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $this->MODULE_ID . "/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true, true);

        // Предполагая, что у вас есть компонент для копирования
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $this->MODULE_ID . "/install/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components", true, true);

        // Предполагая, что у вас есть другие файлы или каталоги для копирования в /shakl
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $this->MODULE_ID . "/install/shakl", $_SERVER["DOCUMENT_ROOT"] . "/shakl", true, true);

        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/install/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
        DeleteDirFilesEx("/bitrix/components/shakl/"); // Если у вас есть компонент для удаления
        DeleteDirFilesEx("/shakl/"); // Если у вас есть другие файлы или каталоги для удаления в /shakl

        return true;
    }
}
