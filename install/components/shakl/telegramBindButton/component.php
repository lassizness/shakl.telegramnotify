<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!\Bitrix\Main\Loader::includeModule('shakl.telegramnotify')) {
    ShowError(GetMessage("SHAKL_TELEGRAMNOTIFY_MODULE_NOT_INSTALLED"));
    return;
}

$botName = COption::GetOptionString("shakl.telegramnotify", "BOT_NAME");
if (empty($botName)) {
    ShowError(GetMessage("SHAKL_TELEGRAM_CHATID_BOT_NAME_NOT_SET"));
    return;
}

$arResult["BOT_NAME"] = $botName;

$this->IncludeComponentTemplate();