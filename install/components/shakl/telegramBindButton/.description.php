<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("COMPONENT_NAME"), // убедитесь, что такой ключ есть в вашем языковом файле
    "DESCRIPTION" => GetMessage("COMPONENT_DESCRIPTION"), // то же самое
    "PATH" => array(
        "ID" => "shakl",
        "NAME" => "SHAKL",
    ),
);