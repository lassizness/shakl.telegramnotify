<?php
$MESS["SHAKL_TELEGRAM_CHATID_BUTTON_NAME"] = "Кнопка привязки Telegram ChatID";
$MESS["SHAKL_TELEGRAM_CHATID_BUTTON_DESC"] = "Добавляет кнопку для привязки Telegram ChatID пользователя.";
$MESS["SHAKL_TELEGRAM_CHATID_BUTTON_NAMESPACE"] = "SHAKL";
$MESS["SHAKL_TELEGRAMNOTIFY_TELEGRAM_CHATID_BUTTON_TEXT"] = "Привязать Telegram";
