<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $DB;
$userId = $GLOBALS["USER"]->GetID();
$code = $component->generateVerificationCode($userId);

$res = $DB->Query("SELECT CHAT_ID, VERIFICATION_CODE FROM b_user_telegram WHERE USER_ID={$userId}");
$chatIsBound = false;
$verificationCodeExists = false;

if ($row = $res->Fetch()) {
    if (!empty($row['CHAT_ID'])) {
        $chatIsBound = true;
    }
    if ($row['VERIFICATION_CODE'] == $code) {
        $verificationCodeExists = true;
    }
}

if ($chatIsBound) {
    echo GetMessage("SHAKL_TELEGRAM_CHATID_BUTTON_UNBIND_TEXT");
} else {
    if (!$verificationCodeExists) {
        if ($res->SelectedRowsCount() > 0) {
            $DB->Query("UPDATE b_user_telegram SET VERIFICATION_CODE='{$code}' WHERE USER_ID={$userId}");
        } else {
            $DB->Query("INSERT INTO b_user_telegram (USER_ID, VERIFICATION_CODE) VALUES ({$userId}, '{$code}')");
        }
    }

    $botName = COption::GetOptionString("shakl.telegramnotify", "BOT_NAME");
    echo "Привязать аккаунт к Telegram?<br>";
    // Инпут с хеш-суммой
    echo "<input type='text' id='hashInput' value='{$code}' readonly>";

    // Кнопка для копирования хеш-суммы
    echo "<a class='buttoncopy' onclick='copyToClipboard()'>Копировать код</a><br>";

    echo '<a class="buttont" href="https://t.me/' . $botName . '?start=' . $code . '" target="_blank">Отправить код боту</a>';
}
?>

<script>
    function copyToClipboard() {
        var hashInput = document.getElementById('hashInput');
        hashInput.select();
        document.execCommand("copy");
        alert("Код скопирован!"); // Оповещение о том, что текст был скопирован
    }
</script>

<style>
    /* CSS стили для инпута и кнопки */
    #hashInput {
        border: 1px solid #ccc;
        padding: 8px;
        margin: 10px 0;
    }
    .buttoncopy {
        display: unset!important;
        background-color: #5d8ac1;
        color: white;
        padding: 10px 15px;
        border: none;
        cursor: pointer;
        border-radius: 10px;
    }
    .buttont {
        display: unset!important;
        background-color: #eb754e;
        color: white;
        padding: 10px 15px;
        border: none;
        cursor: pointer;
        border-radius: 10px;
    }
    button:hover {
        background-color: #41689b;
    }
</style>
