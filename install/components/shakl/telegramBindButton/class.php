<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

Loc::loadMessages(__FILE__);

class TelegramBindButtonComponent extends CBitrixComponent
{
    public function executeComponent()
    {
        if (!CModule::IncludeModule('shakl.telegramnotify')) {
            ShowError("Модуль shakl.telegramnotify не установлен");
            return;
        }

        $this->arResult['IS_BOUND'] = $this->isUserBound();
        $this->includeComponentTemplate();
    }

    private function isUserBound()
    {
        global $DB, $USER;

        $userId = $USER->GetID();
        $res = $DB->Query("SELECT CHAT_ID FROM b_user_telegram WHERE USER_ID=$userId");
        return (bool) $res->Fetch();
    }

    public function generateVerificationCode($userId)
    {
        return md5($userId . time() . Option::get("shakl.telegramnotify", "API_KEY"));
    }
}
