<?php
$arComponentDescription = array(
    "NAME" => "Проверка привязки Telegram",
    "DESCRIPTION" => "Проверяет привязку пользователя к Telegram",
    "PATH" => array(
        "ID" => "shakl",
        "CHILD" => array(
            "ID" => "telegram",
            "NAME" => "Telegram"
        )
    ),
);
