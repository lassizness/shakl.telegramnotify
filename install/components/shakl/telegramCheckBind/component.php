<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Проверка на установленный модуль
if (!CModule::IncludeModule('shakl.telegramnotify')) {
    ShowError("Модуль 'shakl.telegramnotify' не установлен!");
    return;
}

// Получение ID текущего пользователя
$userId = $GLOBALS["USER"]->GetID();

if (!$userId) {
    $arResult["CHAT_ID"] = false; // пользователь не авторизован
} else {
    global $DB;

    // Запрашиваем данные о пользователе из таблицы b_user_telegram
    $res = $DB->Query("SELECT CHAT_ID FROM b_user_telegram WHERE USER_ID={$userId}");

    if ($row = $res->Fetch()) {
        if (!empty($row['CHAT_ID'])) {
            $arResult["CHAT_ID"] = $row['CHAT_ID']; // возвращает CHAT_ID если он существует
        } else {
            $arResult["CHAT_ID"] = false; // CHAT_ID не существует
        }
    } else {
        $arResult["CHAT_ID"] = false; // запись не найдена
    }
}

// Подключаем шаблон компонента
$this->IncludeComponentTemplate();
