<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/shakl.telegramnotify/include.php");
// Проверка прав доступа
if(!$USER->IsAdmin())
    return;

IncludeModuleLangFile(__FILE__);

$errorMessage = ""; // Инициализируем переменную для ошибки

// Загрузите текущие ассоциации из опций модуля
$eventAssociations = COption::GetOptionString("shakl.telegramnotify", "EVENT_ASSOCIATIONS", []);
if($eventAssociations) {
    $eventAssociations = unserialize($eventAssociations);
} else {
    $eventAssociations = [];
}

// Загрузите список событий из настроек модуля
$arSaleEvents = COption::GetOptionString("shakl.telegramnotify", "EVENTS", []);
if($arSaleEvents) {
    $events = json_decode($arSaleEvents , true);
} else {
    $arSaleEvents = [];
}

$allEvents = [];
foreach ($events as $moduleEvents) {
    foreach ($moduleEvents as $event) {
        $allEvents[] = [
            'eventName' => $event["event"],
            'eventClass' => $event["class"]
        ];
    }
}


// Сохранение ассоциаций после POST запроса
if ($_SERVER["REQUEST_METHOD"] == "POST" && check_bitrix_sessid()) {
    if (isset($_POST["removeAssociation"])) {
        $idToRemove = $_POST["removeAssociation"];
        foreach ($eventAssociations as $key => $assoc) {
            if ($assoc['id'] == $idToRemove) {
                unset($eventAssociations[$key]);
                break;
            }
        }
    }

    if (isset($_POST["addAssociation"])) {
        $newEvent = $_POST["newEvent"];
        $newGroup = $_POST["newGroup"];

        // Определение модуля на основе выбранного события
        $newModule = '';
        $newEventClass = '';
        foreach ($events as $moduleName => $moduleEvents) {
            foreach ($moduleEvents as $eventDetails) {
                if ($eventDetails["event"] == $newEvent) {
                    $newModule = $moduleName;
                    $newEventClass = $eventDetails["class"];
                    break 2; // завершаем оба цикла, так как нашли нужное событие
                }
            }
        }

        // Проверяем на дублирование ассоциаций
        $isDuplicate = false;
        foreach ($eventAssociations as $assoc) {
            if ($assoc['event'] == $newEvent && $assoc['group'] == $newGroup) {
                $isDuplicate = true;
                break;
            }
        }

        if (!$isDuplicate) {
            // Генерируем уникальный ID для ассоциации (можно заменить на что-то другое)
            $id = md5($newEvent . $newGroup . time());
            $eventAssociations[] = [
                'id' => $id,
                'module' => $newModule, // теперь модуль определен автоматически
                'eventClass' => $newEventClass, // добавляем класс обработчика
                'event' => $newEvent,
                'group' => $newGroup
            ];
            COption::SetOptionString("shakl.telegramnotify", "EVENT_ASSOCIATIONS", serialize($eventAssociations));
            $event = new \Bitrix\Main\Event('shakl.telegramnotify', 'OnAfterSettingsChange', ['associations' => $eventAssociations]);
            $event->send();

        } else {
            $errorMessage = "Ассоциация для этого события и группы уже существует!";
        }

    }

    COption::SetOptionString("shakl.telegramnotify", "EVENT_ASSOCIATIONS", serialize($eventAssociations));
    $event = new \Bitrix\Main\Event('shakl.telegramnotify', 'OnAfterSettingsChange', ['associations' => $eventAssociations]);
    $event->send();
}

// Получение списка групп пользователей
$arGroups = [];
$rsGroups = CGroup::GetList(($by="c_sort"), ($order="desc"), ["ACTIVE" => "Y"]);
while ($arGroup = $rsGroups->Fetch()) {
    $arGroups[] = $arGroup;
}

$groupNamesById = [];
foreach ($arGroups as $group) {
    $groupNamesById[$group["ID"]] = $group["NAME"];
}

$eventClassByEventName = [];
foreach ($allEvents as $event) {
    $eventClassByEventName[$event['eventName']] = $event['eventClass'];
}

$APPLICATION->SetTitle("Настройки ассоциации событий");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<form method="POST" action="<?= $APPLICATION->GetCurPage() ?>">
    <?= bitrix_sessid_post() ?>

    <!-- Выводим текущие ассоциации -->
    <?php if (!empty($eventAssociations)) {
        ?>
        <table class="adm-list-table">
            <thead>
            <tr class="adm-list-table-header">
                <td class="adm-list-table-cell">
                    <div class="adm-list-table-cell-inner">ID</div>
                </td>
                <td class="adm-list-table-cell">
                    <div class="adm-list-table-cell-inner">Модуль</div>
                </td>
                <td class="adm-list-table-cell">
                    <div class="adm-list-table-cell-inner">Событие</div>
                </td>
                <td class="adm-list-table-cell">
                    <div class="adm-list-table-cell-inner">Класс обработчика</div>
                </td>
                <td class="adm-list-table-cell">
                    <div class="adm-list-table-cell-inner">Группа</div>
                </td>
                <td class="adm-list-table-cell">
                    <div class="adm-list-table-cell-inner">Действие</div>
                </td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($eventAssociations as $association){
                $groupName = $groupNamesById[$association['group']] ?? 'Неизвестная группа';
                ?>

                <tr class="adm-list-table-row">
                    <td class="adm-list-table-cell"><?= $association['id'] ?></td>
                    <td class="adm-list-table-cell"><?= $association['module'] ?></td>
                    <td class="adm-list-table-cell"><?= $association['event'] ?></td>
                    <td class="adm-list-table-cell"><?= $eventClassByEventName[$association['event']] ?? 'Не указан' ?></td>
                    <td class="adm-list-table-cell"><?= $groupName ?> (<?= $association['group'] ?>)</td>
                    <td class="adm-list-table-cell">
                        <button type="submit" class="adm-btn" name="removeAssociation" value="<?= $association['id'] ?>">Удалить</button>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php }else{ ?>
        <p>Ассоциаций пока нет.</p>
    <?php } ?>

    <!-- Форма для добавления новой ассоциации -->
    <div class="adm-detail-content-item-block">
        <h3>Добавить новую ассоциацию</h3>
        <div>
            <select name="newEvent">
                <?php foreach ($allEvents as $event): ?>
                    <option value="<?= $event['eventName'] ?>"><?= $event['eventName'] ?></option>
                <?php endforeach; ?>
            </select>

            <select name="newGroup">
                <?php foreach ($arGroups as $group): ?>
                    <option value="<?= $group["ID"] ?>"><?= $group["NAME"] ?></option>
                <?php endforeach; ?>
            </select>

            <button type="submit" class="adm-btn-save" name="addAssociation">Добавить ассоциацию</button>
        </div>
        <!-- Вывод ошибки под кнопками -->
        <?php if ($errorMessage): ?>
            <div class='error'><?= $errorMessage ?></div>
        <?php endif; ?>
    </div>
</form>

<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>
