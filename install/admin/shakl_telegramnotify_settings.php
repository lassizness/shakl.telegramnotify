<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if (!$USER->IsAdmin()) {
    return;
}

IncludeModuleLangFile(__FILE__);

function convertFilePathToUrl($filePath) {
    $documentRoot = $_SERVER['DOCUMENT_ROOT'];
    $relativePath = str_replace($documentRoot, '', $filePath);
    $protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') ? "https://" : "http://";
    $domain = $_SERVER['HTTP_HOST'];
    return $protocol . $domain . $relativePath;
}

function registerWebhookFunction($webhookPathOrUrl) {
    // Получаем API ключ из настроек модуля
    $apiKey = COption::GetOptionString("shakl.telegramnotify", "API_KEY");

    // Проверяем, является ли введенное значение URL
    if (filter_var($webhookPathOrUrl, FILTER_VALIDATE_URL)) {
        $webhookUrl = $webhookPathOrUrl;
    } else {
        // Добавьте начальный слэш, если его нет
        if ($webhookPathOrUrl[0] !== '/') {
            $webhookPathOrUrl = '/' . $webhookPathOrUrl;
        }
        // Преобразуйте путь файла в URL
        $webhookUrl = convertFilePathToUrl($webhookPathOrUrl);
    }

    // Проверка URL с помощью cURL
    $ch = curl_init($webhookUrl);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);
    $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($responseCode != 200) {
        return "URL вебхука не возвращает статус 200. Полученный статус: " . $responseCode;
    }

    // Генерируем URL для установки вебхука в Telegram
    $telegramSetWebhookUrl = "https://api.telegram.org/bot" . $apiKey . "/setWebhook?url=" . $webhookUrl;

    // Отправляем GET-запрос к API Telegram
    $response = file_get_contents($telegramSetWebhookUrl);

    if ($response === FALSE) {
        return "Ошибка при регистрации вебхука в Telegram";
    }

    // Если требуется, вы можете декодировать и проверить ответ от API Telegram
    $responseData = json_decode($response, true);
    if ($responseData['ok']) {
        // Сохраняем путь к файлу после успешной регистрации
        COption::SetOptionString("shakl.telegramnotify", "WEBHOOK_PATH",  $webhookUrl);
        return "Вебхук успешно зарегистрирован в Telegram с URL: $webhookUrl";
    } else {
        return "Ошибка от Telegram: " . $responseData['description'];
    }
}


function deleteWebhookFunction() {
    // Получаем API ключ из настроек модуля
    $apiKey = COption::GetOptionString("shakl.telegramnotify", "API_KEY");

    // Генерируем URL для удаления вебхука в Telegram
    $telegramDeleteWebhookUrl = "https://api.telegram.org/bot" . $apiKey . "/deleteWebhook";

    // Отправляем GET-запрос к API Telegram
    $response = file_get_contents($telegramDeleteWebhookUrl);

    if ($response === FALSE) {
        return "Ошибка при удалении вебхука в Telegram";
    }

    // Если требуется, вы можете декодировать и проверить ответ от API Telegram
    $responseData = json_decode($response, true);
    if ($responseData['ok']) {
        // Удаляем путь к файлу после успешного удаления вебхука
        COption::SetOptionString("shakl.telegramnotify", "WEBHOOK_PATH", "");
        return "Вебхук успешно удален из Telegram";
    } else {
        return "Ошибка от Telegram: " . $responseData['description'];
    }
}


$message = ""; // для вывода результата регистрации вебхука

if ($_SERVER["REQUEST_METHOD"] == "POST" && check_bitrix_sessid()) {
    if (isset($_POST["register_webhook"])) {
        $message = registerWebhookFunction($_POST["WEBHOOK_PATH"]);
    } else {
        COption::SetOptionString("shakl.telegramnotify", "API_KEY", $_POST["API_KEY"]);
        COption::SetOptionString("shakl.telegramnotify", "BOT_NAME", $_POST["BOT_NAME"]);
    }
    if (isset($_POST["delete_webhook"])) {
        $message = deleteWebhookFunction();
    }
}

$apiKey = COption::GetOptionString("shakl.telegramnotify", "API_KEY");
$botName = COption::GetOptionString("shakl.telegramnotify", "BOT_NAME");
$webhookPath = COption::GetOptionString("shakl.telegramnotify", "WEBHOOK_PATH");

$aTabs = array(
    array("DIV" => "edit1", "TAB" => "Настройки", "ICON" => "main_user_edit", "TITLE" => "Настройки модуля")
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
$APPLICATION->SetTitle("Настройки модуля");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
<form method="POST" action="<?= $APPLICATION->GetCurPage() ?>">
    <?= bitrix_sessid_post() ?>

    <?php
    $tabControl->Begin();
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td width="40%">API Key:</td>
        <td width="60%"><input type="text" size="50" name="API_KEY" value="<?= htmlspecialchars($apiKey) ?>"></td>
    </tr>
    <tr>
        <td width="40%">Bot name :</td>
        <td width="60%"><input type="text" size="50" name="BOT_NAME" value="<?= htmlspecialchars($botName) ?>"></td>
    </tr>
    <tr>
        <td width="40%">Путь к webhook:</td>
        <td width="60%">
            <input type="text"  size="50"  name="WEBHOOK_PATH" value="<?= htmlspecialchars($webhookPath) ?>">
        </td>
    </tr></tr>

    <?php
    $tabControl->EndTab();
    $tabControl->Buttons();
    ?>

    <input type="submit" name="save" value="Сохранить">
    <input type="submit" name="register_webhook" value="Регистрация Webhook">
    <input type="submit" name="delete_webhook" value="Удаление Webhook">
    <?php
    $tabControl->End();
    ?>
</form>


<!-- Выводим результат регистрации вебхука -->
<?php if ($message): ?>
    <p><?= $message ?></p>
<?php endif; ?>

<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>
