<?php

use Bitrix\Main\ModuleManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\EventManager;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\DB\Connection;

class CShaklTelegramNotifyHandlers
{
    function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        $MODULE_ID = 'shakl.telegramnotify';

        // Если раздел не существует, добавляем его
        if (!isset($aGlobalMenu['global_menu_shakl'])) {
            $aGlobalMenu['global_menu_shakl'] = array(
                'menu_id' => 'shakl',
                'text' => 'ШАКЛ',
                'title' => 'Меню модулей ШАКЛ',
                'sort' => 1000,
                'items_id' => 'global_menu_shakl',
                'items' => array()
            );
        }

        // добавляем ссылку на страницу настроек модуля в созданный раздел
        $aModuleMenu[] = array(
            'parent_menu' => 'global_menu_shakl',
            'icon' => '',
            'page_icon' => '',
            'sort' => '200',
            'text' => 'Модуль TelegramNotify',
            'title' => 'Модуль TelegramNotify',
            'url' => '/bitrix/admin/shakl_telegramnotify_settings.php',
            'items_id' => 'menu_shakl_telegramnotify',
            'items' => array(
                array(
                    'text' => 'Настройки',
                    'url' => '/bitrix/admin/shakl_telegramnotify_settings.php',
                    'title' => 'Настройки модуля TelegramNotify',
                    'more_url' => array(),
                    'items_id' => 'menu_shakl_telegramnotify_settings',
                ),
                array(
                    'text' => 'Уведомления',
                    'url' => '/bitrix/admin/shakl_event_notification_settings.php',
                    'title' => 'Уведомления о событиях групп пользователей',
                    'more_url' => array(),
                    'items_id' => 'menu_shakl_event_notification__settings',
                ),
                // Можете добавить другие пункты меню по аналогии, если они будут нужны в будущем.
            )
        );
    }
}

class TelegramNotify
{

    protected static function getConnection(): Connection
    {
        return Application::getConnection();
    }

    public static function setChatId($userId, $chatId)
    {
        self::getConnection()->queryExecute("REPLACE INTO b_user_telegram (USER_ID, CHAT_ID) VALUES ($userId, '$chatId')");
    }

    public static function getChatIdByUserId($userId)
    {
        $res = self::getConnection()->query("SELECT CHAT_ID FROM b_user_telegram WHERE USER_ID=$userId");
        if ($row = $res->fetch()) {
            return $row["CHAT_ID"];
        }
        return false;
    }

    public static function sendNotification($userId, $message, $botToken)
    { $botToken=COption::GetOptionString("shakl.telegramnotify", "API_KEY");

        $res = self::getConnection()->query("SELECT CHAT_ID FROM b_user_telegram WHERE USER_ID=$userId");
        if ($row = $res->fetch()) {
            $chatId = $row["CHAT_ID"];
            $httpClient = new HttpClient();
            $apiUrl = "https://api.telegram.org/bot{$botToken}/sendMessage";
            $postData = [
                'chat_id' => $chatId,
                'text' => $message,
            ];
            $httpClient->post($apiUrl, $postData);
        }
    }

    public static function logData($data)
    {
        $logFilePath = $_SERVER["DOCUMENT_ROOT"] . "/telegram_webhook_log33.txt";
        $logEntry = "[" . date("Y-m-d H:i:s") . "] " . print_r($data, true) . "\n\n";
        file_put_contents($logFilePath, $logEntry, FILE_APPEND);
    }
}

class WebhookHandler
{
    protected function getConnection(): Connection
    {
        return Application::getConnection();
    }

    private function deleteUserMessage($chat_id, $message_id, $botToken)
    {
        $apiUrl = "https://api.telegram.org/bot{$botToken}/deleteMessage";
        $postData = [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
        ];
        $httpClient = new HttpClient();
        $httpClient->post($apiUrl, $postData);
    }

    public function handle($data)
    {
        $botToken = \COption::GetOptionString("shakl.telegramnotify", "API_KEY");
        $httpClient = new HttpClient();

        if (isset($data['message'])) {
            $chat_id = $data['message']['chat']['id'];

            if ($this->isChatIdBoundToUser($chat_id)) {
                $this->deleteUserMessage($chat_id, $data['message']['message_id'], $botToken);
                return;
            }

            $text = $data['message']['text'];


            if (strpos($text, '/start') === 0) {
                $apiUrl = "https://api.telegram.org/bot{$botToken}/sendMessage";
                $postData = [
                    'chat_id' => $chat_id,
                    'text' => "Пожалуйста, введите ваш уникальный код верификации.",
                ];
                $httpClient->post($apiUrl, $postData);
            } elseif (preg_match('/^[a-f0-9]{32}$/', $text)) {
                $userId = $this->getUserIdByVerificationCode($text);
                if ($userId) {
                    TelegramNotify::setChatId($userId, $chat_id);
                    TelegramNotify::sendNotification($userId, "Вы успешно привязали свой аккаунт!", $botToken);
                   // TelegramNotify::logData("После успешного подтверждения Chat ID");
                    // После успешного подтверждения Chat ID
                    $event = new \Bitrix\Main\Event('shakl.telegramnotify', 'OnChatIDConfirmed', ['USER_ID' => $userId, 'CHAT_ID' => $chat_id]);
                    $event->send();
                    // Проверка результата
                    if ($event->getResults()) {
                       // TelegramNotify::logData("Событие было успешно обработано одним или несколькими обработчиками");
                        foreach ($event->getResults() as $eventResult) {
                            if ($eventResult->getType() == \Bitrix\Main\EventResult::ERROR) {
                                $errorMessages = $eventResult->getParameters();
                                //TelegramNotify::logData("В одном из обработчиков произошла ошибка: ".$errorMessages);
                            }
                        }
                    }
                } else {
                    // Отправляем сообщение напрямую на chat_id
                    $apiUrl = "https://api.telegram.org/bot{$botToken}/sendMessage";
                    $postData = [
                        'chat_id' => $chat_id,
                        'text' => "Неверный код верификации. Пожалуйста, попробуйте еще раз.",
                    ];
                    $httpClient->post($apiUrl, $postData);
                }
            }
        }
    }

    private function getUserIdByVerificationCode($code)
    {
        $safeCode = $this->getConnection()->getSqlHelper()->forSql($code);
        $res = $this->getConnection()->query("SELECT USER_ID FROM b_user_telegram WHERE VERIFICATION_CODE='{$safeCode}'");
        if ($row = $res->fetch()) {
            return $row["USER_ID"];
        }
        return false;
    }

    private function isChatIdBoundToUser($chat_id)
    {
        $safeChatId = $this->getConnection()->getSqlHelper()->forSql($chat_id);
        $res = $this->getConnection()->query("SELECT USER_ID FROM b_user_telegram WHERE CHAT_ID='{$safeChatId}'");
        return (bool) $res->fetch();
    }
}

class EventNotificationHandler {

    private $eventAssociations;

    public function __construct() {
        $this->loadEventAssociations();
    }

    private function loadEventAssociations() {
        $this->eventAssociations = COption::GetOptionString("shakl.telegramnotify", "EVENT_ASSOCIATIONS", []);
        if ($this->eventAssociations) {
           // TelegramNotify::logData($this->eventAssociations. ": loadEventAssociations");
            $this->eventAssociations = unserialize($this->eventAssociations);
        } else {
            $this->eventAssociations = [];
            //TelegramNotify::logData("хуй");
        }
    }
    public function registerHandlers() {
        foreach ($this->eventAssociations as $association) {
            $eventName = $association['event'];
            $module = $association['module'];

            $handlerInstance = $this;
            $callback = function($event) use ($handlerInstance, $eventName) {
                $handlerInstance::StaticEventHandler($eventName, $event);
            };

            AddEventHandler($module, $eventName, $callback);
           // TelegramNotify::logData($module . " " . $eventName . " " . __CLASS__ . ": зарегистрировано");
        }
    }


    public function unregisterHandlers() {
        foreach ($this->eventAssociations as $association) {
            $eventName = $association['event'];
            $module = $association['module'];
            UnRegisterModuleDependences($module, $eventName, __CLASS__, 'StaticEventHandler');
           // TelegramNotify::logData($module." ".$eventName." ".__CLASS__.": Разрегистрировано");
        }
    }

    public static function StaticEventHandler($eventName, $event) {
        $handler = new self();
        $type = get_class($event); // Получаем полное имя класса объекта
        $parameters = ($event instanceof \Bitrix\Main\Event) ? $event->getParameters() : $event; // Если это не Bitrix\Main\Event, то мы просто преобразуем объект в массив
        $handler->universalEventHandler($type, $eventName, $parameters);
    }

    private function universalEventHandler($type, $eventName, $parameters) {
        TelegramNotify::logData(__CLASS__ . " " . $type . ": Entered");
        switch ($type) {
            case \Bitrix\Main\Event::class:
                // Обработка событий типа Bitrix\Main\Event
                $eventName = $parameters['event_type']; // Это просто пример. Фактическая структура может отличаться.
                break;
            case \Bitrix\Sale\Order::class:
                    $orderId = $parameters->getId();
                    $price = $parameters->getPrice();
                    $currency = $parameters->getCurrency();
                    $message = "Заказ создан {$orderId}. Сумма: {$price} {$currency}.";

                break;
            case \Bitrix\Sale\Basket::class:
                // Обработка событий типа Bitrix\Sale\Basket
                $basketId = $parameters['ID'];
                $message="Корзина обновлена ".$basketId.".";
                break;
            case \Bitrix\Sale\BasketItem::class:
                // Обработка событий типа Bitrix\Sale\BasketItem
                $basketItemId = $parameters['ID'];
                $message="Товар в корзине обновлен ".$basketItemId.".";
                break;
            case \Bitrix\Sale\Payment::class:
                // Обработка событий типа Bitrix\Sale\Payment
                $paymentId = $parameters['ID'];
                $message="Оплата обновлена ".$paymentId.".";
                break;
            default:
                TelegramNotify::logData(__CLASS__ . ": Unknown event type");
                return;
        }

        foreach ($this->eventAssociations as $association) {
            TelegramNotify::logData($eventName. ":". $association['event']."валера" );
            if ($association['event'] ==$eventName) {
                $this->sendNotificationToGroup($association['group'],$association['event'], $message);
                TelegramNotify::logData(__CLASS__ . ": Attempting to send notification");
            }
        }
    }

    private function sendNotificationToGroup($groupID, $eventName, $message) {
        TelegramNotify::logData(__CLASS__.":вошли");
        $rsUsers = CUser::GetList(($by="id"), ($order="desc"), ['GROUPS_ID' => $groupID]);
        while ($arUser = $rsUsers->GetNext()) {
            //$message = "Событие: $eventName произошло!";
            TelegramNotify::logData(__CLASS__.":отправили");
            $botToken = \COption::GetOptionString("shakl.telegramnotify", "API_KEY");
            TelegramNotify::sendNotification($arUser["ID"], $message,$botToken );
        }
    }

    public static function objectToArray($object, $depth = 0) {
        if ($depth > 50) { // Устанавливаем максимальное значение глубины, например 10
            return $object;
        }

        if (is_object($object)) {
            $object = (array) $object;
        }

        if (is_array($object)) {
            $new = array();
            foreach ($object as $key => $val) {
                $new[$key] = self::objectToArray($val, $depth + 1);  // Увеличиваем глубину на 1
            }
        } else {
            $new = $object;
        }
        return $new;
    }

}

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'shakl.telegramnotify',
    'OnAfterSettingsChange',
    'onAfterSettingsChangeHandler'
);
function onAfterSettingsChangeHandler(\Bitrix\Main\Event $event) {
    $parameters = $event->getParameters();
    $associations = $parameters['associations'];

    $handler = new EventNotificationHandler();
    $handler->unregisterHandlers();
    $handler->registerHandlers();
}

//\Bitrix\Main\EventManager::getInstance()->addEventHandler("sale", "OnSaleOrderSaved",[ "EventNotificationHandler","StaticEventHandler"]);

/*function TestEventHandler(\Bitrix\Main\Event $event) {
    $order = $event->getParameter("ENTITY");
    $isNew = $event->getParameter("IS_NEW");

    if ($order && $isNew !== null) {
        $orderID = $order->getId();
        TelegramNotify::logData("Поступил заказ с ID: " . $orderID, "", "debug5566.txt");

    }
}

global $APPLICATION;
$originalAddEventHandler = $APPLICATION->GetOriginalHandler('AddEventHandler');
$APPLICATION->AddEventHandler = function($module, $event, $handler, $sort = 100) use ($originalAddEventHandler) {
    // Ваш код, например, запись в лог:

    // Запись или обработка каждого добавленного обработчика
    TelegramNotify::logData("Event: $event in Module: $module\n");
    // Вызов оригинальной функции
    return $originalAddEventHandler($module, $event, $handler, $sort);
};*/
